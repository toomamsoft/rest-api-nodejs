"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = require("../config");
const route_1 = require("./route");
const google_places_web_1 = require("google-places-web");
class SCGRoute extends route_1.BaseRoute {
    static create(router) {
        router.post('/scg/restaurants', (req, res, next) => {
            new SCGRoute().findingallrestaurants(req, res, next);
        });
    }
    constructor() {
        super();
    }
    findingallrestaurants(req, res, next) {
        try {
            google_places_web_1.default.apiKey = 'AIzaSyBFW4pv9XnMmDskQkXHfjDko7oz-6fkbz4';
            google_places_web_1.default.nearbysearch({
                location: `13.82820,100.52969`,
                Radius: `1000`,
                type: ["cafe", "bar", "restaurant", "food", "establishment"],
                keyword: 'restaurant',
                rankby: "distance"
            }).then(result => {
                res.json(result);
            }).catch((e => {
                res.status(404).send(e);
            }));
        }
        catch (ex) {
            config.Logger(ex);
            res.send(ex);
        }
    }
}
exports.SCGRoute = SCGRoute;
