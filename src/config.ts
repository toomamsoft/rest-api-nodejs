import * as fs from 'fs';
import * as crypto from 'crypto';

export const db = {
    user: 'sa', 
    password: `sa1234`, 
    server: 'localhost',
    database: 'DB_BKF',
    connectionTimeout: 5000,
    options: {encrypt: true, appName: 'BKF'}
}

export function Logger(message){
    try {
        let date = new Date();
        let filename = "app_log/SystemOut_"+ date.toLocaleDateString() +".log"
        var stream = fs.createWriteStream(filename, {flags:'a'});
        stream.write("Info:: " + JSON.stringify(message) + "\n");
        stream.end();
    } catch(ex) {
        console.log(ex)
        throw ex;
    }
}

export function getParametor(val)
{
    // let key;
    let param = [];
    let prop: parameters

    for(let item in val)
    {
        prop = new parameters();
        prop.key = item
        prop.val = val[item]
        param.push(prop);
    }

    return param;
}

class parameters
{
    key: string
    val: string
}

export async function Base64ToDisk(base64Image, fileSave)
{
    function decodeBase64Image(dataString) 
    {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        var response:any = {};

        if (matches.length !== 3) 
        {
        return new Error('Invalid input string');
        }

        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');

        return response;
    }

    // Regular expression for image type:
    // This regular image extracts the "jpeg" from "image/jpeg"
    var imageTypeRegularExpression = /\/(.*?)$/;      

    // Generate random string
    // var crypto = require('crypto');
    var seed = crypto.randomBytes(20);
    var uniqueSHA1String = crypto.createHash('sha1').update(seed).digest('hex');

    // var base64Data = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAZABkAAD/4Q3zaHR0cDovL25zLmFkb2JlLmN...';
    var base64Data = base64Image;
    var imageBuffer = await decodeBase64Image(base64Data);
    var userUploadedFeedMessagesLocation = 'uploads/';

    var uniqueRandomImageName = 'image-' + uniqueSHA1String;
    // This variable is actually an array which has 5 values,
    // The [1] value is the real image extension
    var imageTypeDetected = imageBuffer.type.match(imageTypeRegularExpression);

    var userUploadedImagePath = userUploadedFeedMessagesLocation + 
                                uniqueRandomImageName +
                                '.' + 
                                imageTypeDetected[1];

    // Save decoded binary image to disk
    try
    {
        fs.writeFile(userUploadedImagePath, imageBuffer.data, ()=>{
            console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
        })
        return userUploadedImagePath
    }
    catch(error)
    {
        this.Logger(error)
        // console.log('ERROR:', error);
        throw error;
    }
}

export function base64Encode(file) {
    try {
        var body = fs.readFileSync(file);
        return body.toString('base64');
    } catch(error)
    {
        this.Logger(error)
        // console.log('ERROR:', error);
        throw error;
    }
}
